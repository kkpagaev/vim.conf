require("trouble").setup()

vim.keymap.set('n', '<leader>t', "<CMD>TroubleToggle<CR>", { desc = '[T]rouble toggle' })
